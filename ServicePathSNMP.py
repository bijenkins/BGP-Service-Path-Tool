from pysnmp.entity.rfc3413.oneliner import cmdgen
from ServicePathDBFunctions import ServicepathDBFunctions
from pprint import pprint


class ServicePathNetSNMP():
    """docstring for """
    def __init__(self, community, dest_ip):
        self.community = community
        self.dest_ip = dest_ip
        self.servicepathsnmpdict = {}
        self.oids = {
                "snmp_location": "1.3.6.1.2.1.1.6.0",
                "hostname": "1.3.6.1.2.1.1.5.0",
                "sysuptime": "1.3.6.1.2.1.1.3.0",
                "tunnel_count": "1.3.6.1.4.1.9.9.171.1.2.1.1.0",
                "model": "1.3.6.1.2.1.47.1.1.1.1.13.1",
                "cpu_uti_last_one_min": ".1.3.6.1.4.1.9.9.109.1.1.1.1.7.1",
                "snmp_contact": "1.3.6.1.2.1.1.4.0"}
        self.go()

    def go(self):
        """docstring for """
        if not self.community or not self.dest_ip:
            raise ValueError("Required: community and list of destation ip's")

        for destination_router in self.dest_ip:
            self.servicepathsnmpdict[destination_router] = {}
            cmdGen = cmdgen.CommandGenerator()
            errorIndication, errorStatus, errorIndex, varBinds = cmdGen.getCmd(
                cmdgen.CommunityData(self.community),
                cmdgen.UdpTransportTarget((destination_router, 161)),
                self.oids["snmp_location"],
                self.oids["hostname"],
                self.oids["sysuptime"],
                self.oids["model"],
                self.oids["cpu_uti_last_one_min"],
                self.oids["tunnel_count"],
                self.oids["snmp_contact"]
            )
            # Check for errors and print out results

            if errorIndication:
                print(errorIndication)
                temp_db_touch = ServicepathDBFunctions()
                self.out = temp_db_touch.update_service_endpoint(
                    destination_router,
                    'Unreachable',
                    'Unreachable',
                    'Unreachable',
                    'Unreachable',
                    None,
                    None,
                    None)
            else:
                if errorStatus:
                    print('%s at %s' % (
                        errorStatus.prettyPrint(),
                        'error here',
                        errorIndex and varBinds[int(errorIndex)-1] or '?'
                        )
                    )
                    temp_db_touch = ServicepathDBFunctions()
                    self.out = temp_db_touch.update_service_endpoint(
                        destination_router,
                        'OID N/A',
                        'OID N/A',
                        'OID N/A',
                        'OID N/A',
                        None,
                        None,
                        None)
                else:
                    for name, val in varBinds:
                        temp_key = name.prettyPrint()
                        temp_value = val.prettyPrint()
                        self.servicepathsnmpdict[destination_router] \
                            [temp_key] = temp_value
                    temp_db_touch = ServicepathDBFunctions()
                    self.out = temp_db_touch.update_service_endpoint(
                        destination_router,
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.2.1.1.5.0"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.2.1.1.6.0"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.2.1.1.4.0"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.2.1.47.1.1.1.1.13.1"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.2.1.1.3.0"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.4.1.9.9.171.1.2.1.1.0"],
                        self.servicepathsnmpdict[destination_router]["1.3.6.1.4.1.9.9.109.1.1.1.1.7.1"]
                    )

def runitman():
    temp_one = ServicepathDBFunctions()
    temp_two = temp_one.get_service_endpoints()
    device_list = [x.ip for x in temp_two]
    pprint(device_list)
    return ServicePathNetSNMP(
        "9b58h6d64g",
        device_list)


if __name__ == "__main__":
    runitman()
