from peewee import *
from settings import service_path_db

database = MySQLDatabase(**service_path_db)

class BaseModel(Model):
    class Meta:
        database = database

class ServicePathPaths(BaseModel):
    created_by = CharField()
    date_created = DateTimeField()
    date_edited = DateTimeField()
    service_path_description = TextField(null=True)
    service_path_enabled = IntegerField()
    service_path_ip = CharField(unique=True)
    service_path_name = CharField(unique=True)
    id = IntegerField(primary_key=True)

    class Meta:
        db_table = 'service_path_paths'

class ServicePathTable(BaseModel):
    alt_path_0_ip = CharField(null=True)
    alt_path_0_local_pref = IntegerField(null=True)
    alt_path_0_loopback_ip = CharField(null=True)
    alt_path_0_via_ip = CharField(null=True)
    alt_path_1_ip = CharField(null=True)
    alt_path_1_local_pref = IntegerField(null=True)
    alt_path_1_loopback_ip = CharField(null=True)
    alt_path_1_via_ip = CharField(null=True)
    alt_path_2_ip = CharField(null=True)
    alt_path_2_local_pref = IntegerField(null=True)
    alt_path_2_loopback_ip = CharField(null=True)
    alt_path_2_via_ip = CharField(null=True)
    alt_path_3_ip = CharField(null=True)
    alt_path_3_local_pref = IntegerField(null=True)
    alt_path_3_loopback_ip = CharField(null=True)
    alt_path_3_via_ip = CharField(null=True)
    alt_path_4_ip = CharField(null=True)
    alt_path_4_local_pref = IntegerField(null=True)
    alt_path_4_loopback_ip = CharField(null=True)
    alt_path_4_via_ip = CharField(null=True)
    alt_path_5_ip = CharField(null=True)
    alt_path_5_local_pref = IntegerField(null=True)
    alt_path_5_loopback_ip = CharField(null=True)
    alt_path_5_via_ip = CharField(null=True)
    counter = IntegerField()
    date = DateTimeField()
    error = CharField(null=True)
    service_path_best_path_ip = CharField(null=True)
    service_path_best_path_local_pref = IntegerField(null=True)
    service_path_best_path_via_ip = CharField(null=True)
    service_path_best_path_via_loopback_ip = CharField(null=True)
    service_path = ForeignKeyField(db_column='service_path_id', rel_model=ServicePathPaths, to_field='id')
    sh_command_output = CharField(null=True)
    sh_command_output = CharField(null=True)
    id = IntegerField(primary_key=True)

    class Meta:
        db_table = 'service_path_table'

class ServicePathEndpoints(BaseModel):
    id = IntegerField(primary_key=True)
    cpu_uti_last_one_min = IntegerField(null=True)
    hostname = CharField(null=True)
    ip = CharField(unique=True)
    model = CharField(null=True)
    snmp_contact = CharField(null=True)
    snmp_location = CharField(null=True)
    sysuptime = IntegerField(null=True)
    tunnel_count = IntegerField(null=True)

    class Meta:
        db_table = 'service_path_endpoints'
