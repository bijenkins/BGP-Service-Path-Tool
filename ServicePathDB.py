
from ServicePathModels import ServicePathPaths, ServicePathTable, ServicePathEndpoints
import datetime
import peewee


class ServicepathDBFunctions():

    def __init__(self):
        self.error = False
        self.status = None
        self.insert_dict = {}
        self.insert_output = None
        self.enabled = None
        self.service_path_ip = ()
        self.service_path_name = ()
        self.service_path_description = ()
        self.service_path_enabled = ()
        self.created_by = ()

    def initialize_service_path_path_db(
            self, service_path_ip, service_path_name, service_path_description,
            service_path_enabled, created_by):
        date_created = datetime.datetime.now()
        self.service_path_ip = service_path_ip
        self.service_path_name = service_path_name
        self.service_path_description = service_path_description
        self.service_path_enabled = service_path_enabled
        self.created_by = created_by
        try:
            ServicePathPaths.create(
                service_path_ip=service_path_ip,
                service_path_name=service_path_name,
                service_path_description=service_path_description,
                service_path_enabled=service_path_enabled,
                created_by=created_by, date_created=date_created)
            self.status = 'Service Path Added: {}'.format(service_path_name)
            self.error = 0
            return self.status
        except IntegrityError as out_error:
            output = str(out_error)
            if "Duplicate" in output and "service_path_ip" in output:
                self.status = 'IP is already in use.'
                self.error = True
                return self.status
            if "Duplicate" in output and "service_path_name" in output:
                self.status = 'Name is already in use.'
                self.error = True
                return self.status

    def insert_into_service_path_table_db(
            self,
            service_path_ip,
            service_path_id,
            service_path_name,
            service_path_best_ip=None,
            service_path_best_path_local_pref=None,
            service_path_best_via_ip=None,
            service_path_best_loopback_ip=None):
        """
        Checks service paths in
        """
        date_created = datetime.datetime.now()

        try:
            ServicePathTable.insert(
                service_path_ip=service_path_ip,
                service_path_name=service_path_name,
                service_path_description=service_path_description,
                service_path_enabled=service_path_enabled,
                created_by=created_by,
                date_created=date_created).execute()
            self.status = 'Service Path Added: {}'.format(service_path_name)
            return self.status
        except IntegrityError as out_error:
            output = str(out_error)
            if "Duplicate" in output and "service_path_ip" in output:
                self.status = 'IP is already in use.'
                self.error = True
                return self.status
            if "Duplicate" in output and "service_path_name" in output:
                self.status = 'Name is already in use.'
                self.error = True
                return self.status

    def get_service_paths(self, enabled=True):
        """
        Get the current list of service Paths from the Service Path Path's table
        in the DB.

        Enabled Arg:
            False = All service paths not enabled
            True = All service paths enabled
        """
        self.enabled = enabled
        if self.enabled == False:
            route_list = ServicePathPaths.select()
            return route_list
        elif self.enabled:
            route_list = ServicePathPaths.select() \
                .where(ServicePathPaths.service_path_enabled == 1)
            return route_list
        else:
            raise ValueError(
                'Enabled argument must be set to either True or False')

    def get_or_create_service_endpoint(self, ip):
        """
        Get a service endpoint, if it does not exist, create it.

        This information will feed information into the background
        ServicePathSNMP tool to provide snmp data for best path endpoint.
        Model for this table is as follows:

        class ServicePathEndpoints(BaseModel):
            id = IntegerField(primary_key=True)
            cpu_uti_last_one_min = IntegerField(null=True)
            hostname = CharField(null=True)
            ip = CharField(unique=True)
            model = CharField(null=True)
            snmp_contact = CharField(null=True)
            snmp_location = CharField(null=True)
            sysuptime = IntegerField(null=True)
            tunnel_count = IntegerField(null=True)

            class Meta:
                db_table = 'service_path_endpoints'
        """

        endpoint_out = ServicePathEndpoints.get_or_create(ip=ip)
        return endpoint_out

    def get_service_endpoints(self):
        """
        Get all service endpoints.

        This information will feed information from the ServicePathEnpdoint
        model.

        Model for this table is as follows:

        class ServicePathEndpoints(BaseModel):
            id = IntegerField(primary_key=True)
            cpu_uti_last_one_min = IntegerField(null=True)
            hostname = CharField(null=True)
            ip = CharField(unique=True)
            model = CharField(null=True)
            snmp_contact = CharField(null=True)
            snmp_location = CharField(null=True)
            sysuptime = IntegerField(null=True)
            tunnel_count = IntegerField(null=True)

            class Meta:
                db_table = 'service_path_endpoints'
        """

        endpoint_out = ServicePathEndpoints.select()
        return endpoint_out

    def update_service_endpoint(
            self,
            ip,
            hostname,
            snmp_location,
            snmp_contact,
            model, sysuptime,
            tunnel_count,
            cpu_uti_last_one_min):
        """
        Get all service endpoints.

        This information will feed information from the ServicePathEnpdoint
        model.

        Model for this table is as follows:

        class ServicePathEndpoints(BaseModel):
            id = IntegerField(primary_key=True)
            cpu_uti_last_one_min = IntegerField(null=True)
            hostname = CharField(null=True)
            ip = CharField(unique=True)
            model = CharField(null=True)
            snmp_contact = CharField(null=True)
            snmp_location = CharField(null=True)
            sysuptime = IntegerField(null=True)
            tunnel_count = IntegerField(null=True)

            class Meta:
                db_table = 'service_path_endpoints'
        """
        best_ip = ip
        endpoint_out = ServicePathEndpoints.update(hostname=hostname, snmp_location=snmp_location, snmp_contact=snmp_contact, model=model, sysuptime=sysuptime, tunnel_count=tunnel_count, cpu_uti_last_one_min=cpu_uti_last_one_min).where(ServicePathEndpoints.ip == best_ip)
        endpoint_out.execute()
        return endpoint_out


    def insert_record(self, insert_dict):

        """
        Function to insert record into the service path table.

        Model is as follows:

        class ServicePathTable(BaseModel):
            alt_path_0_ip = CharField(null=True)
            alt_path_0_local_pref = IntegerField(null=True)
            alt_path_0_loopback_ip = CharField(null=True)
            alt_path_0_via_ip = CharField(null=True)
            alt_path_1_ip = CharField(null=True)
            alt_path_1_local_pref = IntegerField(null=True)
            alt_path_1_loopback_ip = CharField(null=True)
            alt_path_1_via_ip = CharField(null=True)
            alt_path_2_ip = CharField(null=True)
            alt_path_2_local_pref = IntegerField(null=True)
            alt_path_2_loopback_ip = CharField(null=True)
            alt_path_2_via_ip = CharField(null=True)
            alt_path_3_ip = CharField(null=True)
            alt_path_3_local_pref = IntegerField(null=True)
            alt_path_3_loopback_ip = CharField(null=True)
            alt_path_3_via_ip = CharField(null=True)
            alt_path_4_ip = CharField(null=True)
            alt_path_4_local_pref = IntegerField(null=True)
            alt_path_4_loopback_ip = CharField(null=True)
            alt_path_4_via_ip = CharField(null=True)
            alt_path_5_ip = CharField(null=True)
            alt_path_5_local_pref = IntegerField(null=True)
            alt_path_5_loopback_ip = CharField(null=True)
            alt_path_5_via_ip = CharField(null=True)
            counter = IntegerField()
            date = DateTimeField()
            error = CharField(null=True)
            service_path_best_path_ip = CharField(null=True)
            service_path_best_path_local_pref = IntegerField(null=True)
            service_path_best_path_via_ip = CharField(null=True)
            service_path_best_path_via_loopback_ip = CharField(null=True)
            service_path = ForeignKeyField(db_column='service_path_id', rel_model=ServicePathPaths, to_field='id')
            sh_command_output = CharField(null=True)
            sh_command_output = CharField(null=True)
            id = IntegerField(primary_key=True)

            class Meta:
                db_table = 'service_path_table'

        """
        self.insert_dict = insert_dict
        row_insert = ServicePathTable.insert(**self.insert_dict).execute()
        self.insert_output = row_insert

    def update_time(self, provided_date, provided_id):

        """
        Function to get the current path information for the provided path.
        self.output_db
        self.update_output_db
        """
        try:
            self.status = 'UPDATED'
            self.update_output_db = ServicePathTable.update(
                date=provided_date,
                counter=ServicePathTable.counter +
                1).where(ServicePathTable.id == provided_id).execute()
        except ServicePathTable.DoesNotExist:
            self.status = 'No Path Present to Update'

    def get_current_path(self, provided_path):

        """
        Function to get the current path information for the provided path.
        self.output_db

        """

        try:
            output_db = ServicePathTable.select() \
                .where(ServicePathTable.service_path == provided_path) \
                .order_by(ServicePathTable.date.desc()).get()
            self.output_db = output_db.__dict__
            self.output_db["_data"]["status"] = "OK"
            return self.output_db
        except ServicePathTable.DoesNotExist:
            self.status = 'No Path Present in Table'
            self.service_path_best_path_ip = None
