# -*- coding: utf-8 -*-
import paramiko
import socket
from settings import sshCred
import datetime
from ServicePathSNMP import runitman
from ServicePathDBFunctions import ServicepathDBFunctions


class CheckServicePaths(object):

    """
    Checks service paths in
    """

    def __init__(self, head_end_router, ssh_user, ssh_password):
        if not head_end_router or not ssh_user or not ssh_password:
            raise ValueError(
                "Required: hub host/ip, device_username, device_password")
        else:
            self.DBOut = ServicepathDBFunctions()
            self.head_end_router = head_end_router
            self.ssh_user = ssh_user
            self.ssh_password = ssh_password
            self.path_output_dict = {
                'alt_path_0_ip': None,
                'alt_path_0_local_pref': None,
                'alt_path_0_loopback_ip': None,
                'alt_path_0_via_ip': None,
                'alt_path_1_ip': None,
                'alt_path_1_local_pref': None,
                'alt_path_1_loopback_ip': None,
                'alt_path_1_via_ip': None,
                'alt_path_2_ip': None,
                'alt_path_2_local_pref': None,
                'alt_path_2_loopback_ip': None,
                'alt_path_2_via_ip': None,
                'alt_path_3_ip': None,
                'alt_path_3_local_pref': None,
                'alt_path_3_loopback_ip': None,
                'alt_path_3_via_ip': None,
                'alt_path_4_ip': None,
                'alt_path_4_local_pref': None,
                'alt_path_4_loopback_ip': None,
                'alt_path_4_via_ip': None,
                'alt_path_5_ip': None,
                'alt_path_5_local_pref': None,
                'alt_path_5_loopback_ip': None,
                'alt_path_5_via_ip': None,
                'service_path_best_path_ip': None,
                'service_path_best_path_local_pref': None,
                'service_path_best_path_via_ip': None,
                'service_path_best_path_via_loopback_ip': None,
                'sh_command_output': None,
                'date': None,
                'error': None,
                'status': None,
                'service_path': None}
            self.checkpath()

    def checkpath(self):

        """
        Full Checkpath function to check.
        Logs into head end router, performing a "show ip bgp" for each path
        in the ServicePathPaths table.

        It's then expected for a output to contain at least one best path.

        Example output:

        HUB#sh ip bgp 10.23.24.0
        BGP routing table entry for 10.23.24.0/28, version 1284157
        Paths: (6 available, best #6, table default)
          Advertised to update-groups:
             1          5          7          13         14         15       20
             22
          Refresh Epoch 1
          Local
            10.8.11.73 from 10.9.0.1 (10.15.1.41)
              Origin IGP, metric 0, localpref 120, valid, internal
              Originator: 10.111.73.1, Cluster list: 10.15.1.41
              rx pathid: 0, tx pathid: 0
          Refresh Epoch 2
          Local
            10.8.11.73 from 10.8.0.1 (10.15.1.41)
              Origin IGP, metric 0, localpref 120, valid, internal
              Originator: 10.111.73.1, Cluster list: 10.15.1.41
              rx pathid: 0, tx pathid: 0
          Refresh Epoch 2
          Local, (Received from a RR-client)
            10.8.11.72 from 10.8.11.72 (10.111.72.1)
              Origin IGP, metric 0, localpref 110, valid, internal
              rx pathid: 0, tx pathid: 0
          Refresh Epoch 3
          Local, (Received from a RR-client)
            10.8.111.72 from 10.8.111.72 (10.111.72.2)
              Origin IGP, metric 0, localpref 105, valid, internal
              rx pathid: 0, tx pathid: 0
          Refresh Epoch 2
          Local, (Received from a RR-client)
            10.8.111.73 from 10.8.111.73 (10.111.73.2)
              Origin IGP, metric 0, localpref 115, valid, internal
              rx pathid: 0, tx pathid: 0
          Refresh Epoch 2
          Local, (Received from a RR-client)
            10.8.11.73 from 10.8.11.73 (10.111.73.1)
              Origin IGP, metric 0, localpref 120, valid, internal, best
              rx pathid: 0, tx pathid: 0x0

        This data is then disected into relevant parts:

                     self.path_output_dict = {
                        'alt_path_0_ip': None,
                        'alt_path_0_local_pref': None,
                        'alt_path_0_loopback_ip': None,
                        'alt_path_0_via_ip': None,
                        'alt_path_1_ip': None,
                        'alt_path_1_local_pref': None,
                        'alt_path_1_loopback_ip': None,
                        'alt_path_1_via_ip': None,
                        'alt_path_2_ip': None,
                        'alt_path_2_local_pref': None,
                        'alt_path_2_loopback_ip': None,
                        'alt_path_2_via_ip': None,
                        'alt_path_3_ip': None,
                        'alt_path_3_local_pref': None,
                        'alt_path_3_loopback_ip': None,
                        'alt_path_3_via_ip': None,
                        'alt_path_4_ip': None,
                        'alt_path_4_local_pref': None,
                        'alt_path_4_loopback_ip': None,
                        'alt_path_4_via_ip': None,
                        'alt_path_5_ip': None,
                        'alt_path_5_local_pref': None,
                        'alt_path_5_loopback_ip': None,
                        'alt_path_5_via_ip': None,
                        'service_path_best_path_ip': None,
                        'service_path_best_path_local_pref': None,
                        'service_path_best_path_via_ip': None,
                        'service_path_best_path_via_loopback_ip': None,
                        'sh_command_output': None,
                        'date': None,
                        'error': None,
                        'status': None,
                        'service_path': None}

        And finally inserted into the database IF the last record's "best path
        ip" is different from the current data retrieved. If the last record is
        the same then the last row for that service path will be updated. If no
        record is present (first initial record) it will bre recognized as
        different from the current and insert the intial row with the current
        data.
        """

        now = datetime.datetime.now()

        command = 'ssh {}@{}\n'.format(self.ssh_user, self.head_end_router)
        passwdsend = self.ssh_password + '\n'
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        ssh.connect(**GrabaCred)

        route_list = self.DBOut.get_service_paths()
        router_channel = ssh.invoke_shell()
        router_channel.settimeout(10)

        router_channel.send(command)
        buff = ''

        while not buff.endswith('Password: '):
            resp = router_channel.recv(9999)
            buff += resp

        # Send the password and wait for a prompt.
        router_channel.send(passwdsend)
        buff = ''

        while not buff.endswith('#'):
            resp = router_channel.recv(99999)
            buff += resp

        router_channel.send('terminal length 0\n')
        buff = ''

        while not buff.endswith('#'):
            resp = router_channel.recv(99999)
            buff += resp

        router_channel.send('!END\n')
        buff = ''

        while not buff.endswith('#'):
            resp = router_channel.recv(99999)
            buff += resp

        self.counter = 0
        for path in route_list:
            self.path_output_dict['status'] = None
            self.counter += 1
            service_name = path.service_path_name

            # Sending the first commmand to Bart
            router_channel.send('sh ip bgp ' + path.service_path_ip + '\n')
            buff = ''

            while not buff.endswith('#'):
                resp = router_channel.recv(99999)
                buff += resp

            # BGP Show full RAW output.
            bgp_show = buff

            self.path_output_dict['date'] = now
            self.path_output_dict['sh_command_output'] = bgp_show
            self.path_output_dict['service_path'] = path.id
            alt_count = 0

            self.bgp_show = bgp_show
            alt_paths = []
            if '%' not in bgp_show:
                bgp_list = bgp_show.split('Local')
                bgp_list.pop(0)

                # Break up the best line for storage and such.
                for bgp in bgp_list:
                    if 'internal, best' in bgp:
                        best_path = bgp.splitlines()
                    else:
                        alt_paths.append(bgp)

                for alt_path in alt_paths:
                    alt_ip_out = 'alt_path_{}_ip'.format(str(alt_count))
                    alt_pref_out = 'alt_path_{}_local_pref'.format(str(alt_count))
                    alt_loop_out = 'alt_path_{}_loopback_ip'.format(str(alt_count))
                    alt_via_out = 'alt_path_{}_via_ip'.format(str(alt_count))

                    alt_path_lines = alt_path.splitlines()
                    alt_path_lines = [x.strip(' ') for x in alt_path_lines]

                    local_pref = alt_path_lines[2]
                    local_pref = local_pref.split(' ')
                    alt_pref = local_pref[5]
                    self.alt_pref = alt_pref.strip(',')

                    alt_path_identifier = 'alternate_path' + str(alt_count)
                    self.path_output_dict[alt_pref_out] = self.alt_pref

                    alt_ip_pile = alt_path_lines[1]
                    alt_ip_list = alt_ip_pile.split(' ')
                    self.alt_ip = alt_ip_list[0]

                    self.path_output_dict[alt_ip_out] = self.alt_ip

                    self.alt_via = alt_ip_list[2]
                    self.path_output_dict[alt_via_out] = self.alt_via
                    alt_loop = alt_ip_list[3]
                    alt_loop = alt_loop.strip('(')
                    self.alt_loop = alt_loop.strip(')')
                    self.path_output_dict[alt_loop_out] = self.alt_loop
                    alt_count += 1

                for x in best_path:
                    best_list = [x.strip(' ') for x in best_path]
                    local_pref = best_path[2]
                    local_pref = local_pref.split(' ')
                    best_pref = local_pref[11]
                    self.best_pref = best_pref.strip(',')
                    self.path_output_dict['service_path_best_path_local_pref'] = self.best_pref
                    best_ip_pile = best_list[1]
                    best_ip_list = best_ip_pile.split(' ')
                    self.best_ip = best_ip_list[0]
                    self.path_output_dict['service_path_best_path_ip'] = self.best_ip
                    self.best_via = best_ip_list[2]
                    self.path_output_dict['service_path_best_path_via_ip'] = self.best_via
                    best_loop = best_ip_list[3]
                    best_loop = best_loop.strip('(')
                    self.best_loop = best_loop.strip(')')
                    self.path_output_dict['service_path_best_path_via_loopback_ip'] = self.best_loop

            else:
                self.path_output_dict['error'] = 'Network not Found'


            # Get or create the best service path record for SNMP info grab.
            self.DBOut \
                .get_or_create_service_endpoint(
                    self.path_output_dict['service_path_best_path_via_loopback_ip'])

            # Ge the most current path.
            current_path_out = self.DBOut.get_current_path(path.id)
            self.current_path_out = current_path_out





            # dictionary output of what is currently in the db is located at
            # current_path_out['_data']

            if current_path_out:
                if current_path_out["_data"]["status"] == 'No Path Present in Table' or \
                        current_path_out["_data"]["service_path_best_path_ip"] != \
                        self.path_output_dict['service_path_best_path_ip']:
                    if 'status' in self.path_output_dict:
                        del self.path_output_dict['status']
                    self.DBOutput = self.DBOut.insert_record(self.path_output_dict)
                else:
                    if 'status' in self.path_output_dict:
                        del self.path_output_dict['status']

                    self.DBOutput = self.DBOut.update_time(now,
                        current_path_out['_data']['id'])
            else:
                del self.path_output_dict['status']
                self.DBOutput = self.DBOut.insert_record(self.path_output_dict)


        # Perfrom SNMP update to the endpoint routers via ServicePathSNMP
        update_snmp = runitman()


def main():
    def_out = CheckServicePaths(**sshCreds)
    print def_out
    print 'Done'

"""
if __name__ == "__main__":
    main()
"""
